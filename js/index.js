
/**
 * Update the tabs with data from different files
 */
$(document).ready(function() {
  $('#Player').load('html/player.html');
  $('#DungeonMaster').load('html/dungeon_master.html');
  clickNav("Player");
});

function openNav() {
    document.getElementById("main_sidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("main_sidenav").style.width = "0";

}

function clickNav(item) {
    var new_content = document.getElementById(item);
    var main_page = document.getElementById("main_page");

    var all = main_page.childNodes;
    for (var i=0; i < all.length; i++) {
	if (all[i].tagName != "DIV")
	    continue
	all[i].style.display = "none";
    }

    new_content.style.display = "block"

    document.getElementById("main_sidenav").style.width = "0";
}
