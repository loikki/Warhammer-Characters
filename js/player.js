/**
 * Container ensuring the existence of elementary stuff
 */
class Character {
    constructor() {
	this.careers = []
    }
}

/**
 * Transform the object to a string
 * @return {string} the object in string
 */
function stringify(obj) {
    var tmp = JSON.stringify(obj);
    return btoa(tmp);
}

var character = new Character();

/**
 * Generate document data from character
 * @param {Character} character - Character to load
 */
function generateDocument() {
    generateCharacterDocument(character);
    generateCareerDocument(character);
}

/**
 * Construct the object from a string
 * @param {string} txt - Object constructed with stringify
 */
function generateFromString(txt) {
    var tmp = atob(txt);
    tmp = JSON.parse(tmp);
    return tmp;
}

/**
 * Construct a character from the document
 */
function generateCharacter() {
    constructorCharacterPart(character);
    constructorCareerPart(character);
}


/**
 * Display the right tab
 * @param {event} event - Event calling the function
 * @param {string} tab_name - Name of clicked tab
 */
function openTab(event, tab_name) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tab_name).style.display = "block";
    event.currentTarget.className += " active";
}

/**
 * Update the tabs with data from different files
 */
$(document).ready(function() {
    $('#Character').load('html/character.html');
    $('#Career').load('html/career.html');
    $('#Items').load('html/items.html');
    $('#Talent').load('html/talent.html');
    $('#Spell').load('html/spell.html');
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
});



/**
 * Generate a list of characters saved in Local Storage
 * and update the select object
 */
function generateListCharacters() {
    var x = document.getElementById("characters");

    // clean select
    var length = x.options.length;
    for (i = 0; i < length; i++) {
	x.options[i] = null;
    }

    // exit if no previous char
    if (localStorage == "undefined")
	return;
    
    // add all chars
    for(i=0; i < localStorage.length; i++) {
	var key = localStorage.key(i);

	var option = document.createElement("option");
	option.text = key;
	option.value = key;
	x.appendChild(option);
    }
}

/**
 * Save current document in local storage
 * and display the save string
 */
function save_character() {
    generateCharacter();
    var tmp = stringify(character);

    var x = document.getElementById("check_save");
    x.innerHTML = tmp;

    localStorage.setItem(character.name, tmp);
}

/**
 * Load a character from local storage
 * and generate document data from it
 */
function load_character() {

    // Get character name
    var x = document.getElementById("characters");
    x = x.options[x.selectedIndex].value;

    // load character
    character = localStorage.getItem(x);
    character = generateFromString(character);

    // generate fields
    generateDocument();
}


/**
 * Load a character from a string
 * and generate document data from it
 */
function load_from_text() {
    var x = document.getElementById("form_char").load_txt;
    // load character
    character = generateFromString(x.value);

    // generate fields
    generateDocument();
}
