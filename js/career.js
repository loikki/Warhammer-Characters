var career_filename = "data/career.xml";
var career_parser;

var languages_filename = "data/languages.xml"
var languages_parser;

var previous_career;

$(document).ready(function() {
    getText();
});

/**
 * Read the xml data related to the career
 */
function getText() {
    $.ajax({
	type: "GET",
	url: career_filename,
	dataType: "text",
	success: function(xml) {
	    // Parse xml data
	    var parser = new DOMParser();
	    career_parser = parser.parseFromString(xml, "text/xml");
	    // generate list careers
	    generateListCareers();

	    // generate document
	    var select = document.getElementById("careers");
	    previous_career = select[select.selectedIndex];
	    loadCareer(previous_career);
	}
    });
    $.ajax({
	type: "GET",
	url: languages_filename,
	dataType: "text",
	success: function(xml) {
	    // Parse xml data
	    var parser = new DOMParser();
	    languages_parser = parser.parseFromString(xml, "text/xml");
	}
    });
}

/**
 * Save and load a different career
 */
function selectCareer() {
    var select = document.getElementById("careers");
    var current_career = select[select.selectedIndex];

    saveCareer(previous_career);
    previous_career = current_career;
    loadCareer(current_career);
}

function getCareerByName(career) {
    var careers = career_parser.getElementsByTagName("career");
    return careers.namedItem(career);
}


/**
 * Load a different career
 * @param {option} career - Career to load
 */
function loadCareer(career) {
    var x = document.getElementById("form_career");

    if (character.careers == undefined) {
	console.log("Setting default careers")
	character.careers = [];
    }
	
    // Check if known
    var known = true;
    var char_car = getCharacterCareer(career);
    if (char_car == undefined)
	known = false;
    
    x.known.checked = known;
    var career_data = getCareerByName(career.value);

    // main stat data
    var main = true;
    setDataInnerHTML(career_data, "weapon_skill", main);
    setDataInnerHTML(career_data, "ballistic_skill", main);
    setDataInnerHTML(career_data, "strength", main);
    setDataInnerHTML(career_data, "toughness", main);
    setDataInnerHTML(career_data, "agility", main);
    setDataInnerHTML(career_data, "intelligence", main);
    setDataInnerHTML(career_data, "will_power", main);
    setDataInnerHTML(career_data, "fellowship", main);

    // secondary stat data
    main = false;
    setDataInnerHTML(career_data, "attacks", main);
    setDataInnerHTML(career_data, "wounds", main);
    setDataInnerHTML(career_data, "strength_bonus", main);
    setDataInnerHTML(career_data, "toughness_bonus", main);
    setDataInnerHTML(career_data, "movement", main);
    setDataInnerHTML(career_data, "magic", main);
    setDataInnerHTML(career_data, "insanity_point", main);
    setDataInnerHTML(career_data, "fate_point", main);

    if (char_car == undefined)
	char_car = new Object();

    // get list talents
    if (character.talents == undefined) {
	console.log("Setting default character talents");
	character.talents = [];
    }
    
    var talents = career_data.getElementsByTagName("talents");
    if (talents.length != 1)
	alert("Career " + career_data.id + " contains multiple or zero tag 'talents'");
    talents = talents[0].children;

    // display all talents
    var to_discover = document.getElementById("waiting_talents");
    var discovered = document.getElementById("obtained_talents");
    to_discover.innerHTML = "";
    discovered.innerHTML = "";
    displayTextAttributes(talents, to_discover, discovered, character.talents, "Talent");

    // get list skills
    if (character.skills == undefined) {
	console.log("Setting default character skills");
	character.skills = [];
    }
    
    var skills = career_data.getElementsByTagName("skills");
    if (skills.length != 1)
	alert("Career " + career_data.id + " contains multiple or zero tag 'skills'");
    skills = skills[0].children;

    // display all skills
    to_discover = document.getElementById("waiting_skills");
    discovered = document.getElementById("obtained_skills");
    to_discover.innerHTML = "";
    discovered.innerHTML = "";
    displayTextAttributes(skills, to_discover, discovered, character.skills, "Skill");

    // display trappings
    var html = document.getElementById("trappings");
    var data = career_data.getElementsByTagName("trappings");
    if (data.length != 1)
	alert("Career " + career_data.id + " contains multiple or zero tag 'trappings'");
    data = data[0].children;
    var onclick = "goToItem";
    displayCareerInformation(html, data, onclick);

    // display entries
    html = document.getElementById("entries");
    data = career_data.getElementsByTagName("entries");
    if (data.length != 1)
	alert("Career " + career_data.id + " contains multiple or zero tag 'entries'");
    data = data[0].children;
    onclick = "goToCareer";
    displayCareerInformation(html, data, onclick);

    // display exits
    html = document.getElementById("exits");
    data = career_data.getElementsByTagName("exits");
    if (data.length != 1)
	alert("Career " + career_data.id + " contains multiple or zero tag 'exits'");
    data = data[0].children;
    onclick = "goToCareer";
    displayCareerInformation(html, data, onclick);
}

/**
 * load required career
 * @param {string} career - Name of the career to load
 */
function goToCareer(career) {
    var select = document.getElementById("careers");

    // save previous career
    previous_career = select[select.selectedIndex];

    // find option
    var select_index = undefined;
    for (var i=0; i<select.children.length; i++) {
	var name = select.children[i].value;
	if (name == career) {
	    select_index = i;
	    break;
	}
	    
    }
    if (select_index == undefined)
	alert("Cannot find career: " + career);
    // set option
    select.selectedIndex = i;
    // reload data
    selectCareer();
}

/**
 * load required item
 * @param{sting} item - Name of the item to load
 */
function goToItem(item) {
    alert("Not Implemented");
}

/**
 * Display a text information (e.g. trappings/entries/exits) in the career form
 * @param {tag} html - the object to update
 * @param {array} data - list of nodes
 * @param {string} onclick - Name of the function to call when clicked
 */
function displayCareerInformation(html, data, onclick) {
    html.innerHTML = "";
    for (var i=0; i<data.length; i++) {
	var name = data[i].attributes["name"].nodeValue;
	var str_name = " <span onclick=\"" + onclick + "('" + name + "')\">" + name + "</span>;";
	html.innerHTML += str_name;
    }
}


/**
 * Get the required character career.
 * If undefined is provided, use the career selected
 * in the form
 * @param {string} career - Name of the career
 * @return {object} The character career
 */
function getCharacterCareer(career) {
    //get selected career if required
    if (career == undefined) {
	var select = document.getElementById("careers");
	career = select[select.selectedIndex];
    }

    //find career
    var known = false;
    var i;
    for(i=0; i<character.careers.length; i++) {
	if (character.careers[i].name == career.value) {
	    known = true;
	    break;
	}
    }

    if (known == false)
	return undefined;

    else
	return character.careers[i];
}

/**
 * Add a talent to the character
 * @param {string} name - Name of the talent
 */
function addTalent(name) {
    var talents = character.talents;
    if (talents.indexOf(name) != -1)
	alert("You already have this talent");
    talents.push(name);
    selectCareer();

    return undefined;
}

/**
 * Remove a talent from the character
 * @param {string} name - Talent name
 */ 
function removeTalent(name) {
    var talents = character.talents;
    var i = talents.indexOf(name);
    if (i == -1)
	alert("Unable to find this talent");
    talents.splice(i, 1);
    selectCareer();

    return undefined;
}

/**
 * Check if need to prompt a choice when adding skill or talent
 * @param {string} name - talent/skill name
 * @return {bool} true if need a prompt
 */
function needPrompt(name) {
    if (name == "Speak Language")
	return true;

    return false;
}

function displayPrompt(name) {
    var answer = prompt(name);
}

/**
 * Add a skill to the character
 * @param {string} name - Skill name
 */
function addSkill(name) {
    var choice = undefined;
    var str_name = name
    if (needPrompt(name)) {
	choice = displayPrompt(name);
	if (choice == undefined)
	    alert("When life asks you to make a choice, do it");
	str_name = name + "(" + choice + ")";
    }

    var skills = character.skills;
    if (skills.indexOf(name) != -1)
	alert("You already have this skill");
    skills.push(str_name);
    selectCareer();

    return undefined;
}

/**
 * Remove a skill from the character
 * @param {string} name - Skill name
 */
function removeSkill(name) {
    var skills = character.skills;
    var i = skills.indexOf(name);
    if (i == -1)
	alert("Unable to find this skill");
    skills.splice(i, 1);
    selectCareer();

    return undefined;
}

/**
 * Add skill or talent to the correct tag along with an onclick function
 * @param {HTMLCollection} elements - Collection of possible elements
 * @param {tag} to_discover - Tag containing the element to discover
 * @param {tag} discovered - Tag containing the element discovered
 * @param {array} list_known - List of known skills or talents
 * @param {string} function_suffix - Suffix of the function to call (e.g. 'Talent' or 'Skill')
 */
function displayTextAttributes(elements, to_discover, discovered, list_known, function_suffix) {
    var choices = [];
    for(var i=0; i < elements.length; i++) {
	var name = elements[i].attributes["name"].nodeValue;
	var choice = elements[i].attributes["choice"];

	// deal with choice
	if (choice != undefined) {
	    choice = choice.nodeValue;
	    // choice already processed
	    if (choices.indexOf(choice) != -1)
		continue;

	    choices.push(choice);
	    var element2 = getCorrespondingChoice(elements, choice, name);
	    var name2 = element2.attributes["name"].nodeValue;

	    // Check if implemented
	    var nber = element2.attributes["number"]
	    if (nber != undefined && nber.value != 1)
		alert("Not implemented: nber=", nber.value);
	    nber = elements[i].attributes["number"]
	    if (nber != undefined && nber.value != 1)
		alert("Not implemented: nber=", nber.value);

	    // get status
	    var element1_known = list_known.indexOf(name) != -1;
	    var element2_known = list_known.indexOf(name2) != -1;

	    // case both known
	    var str_name1;
	    var str_name2;
	    if (element1_known && element2_known) {
		str_name1 = "<span onclick=\"remove" + function_suffix + "('" + name + "')\">" + name + "</span>";
		str_name2 = "<span onclick=\"remove" + function_suffix + "('" + name2 + "')\">" + name2 + "</span>";
		discovered.innerHTML += " " + str_name1 + "; " + str_name2 + ";";
	    }
	    // case both unknown
	    else if (!element1_known && !element2_known) {
		str_name1 = "<span onclick=\"add" + function_suffix + "('" + name + "')\">" + name + "</span>";
		str_name2 = "<span onclick=\"add" + function_suffix + "('" + name2 + "')\">" + name2 + "</span>";
		to_discover.innerHTML += " " + str_name1 + " <b>or</b> " + str_name2 + ";";
	    }
	    // first mix case
	    else if (element1_known && !element2_known) {
		str_name1 = "<span onclick=\"remove" + function_suffix + "('" + name + "')\">" + name + "</span>";
		discovered.innerHTML += " " + str_name1 + ";";
		str_name2 = "<span onclick=\"add" + function_suffix + "('" + name2 + "')\">" + name2 + "</span>";
		to_discover.innerHTML += " (" + str_name2 + ");";
	    }

	    // second mix case
	    else if (!element1_known && element2_known) {
		str_name1 = " <span onclick=\"add" + function_suffix + "('" + name + "')\">" + name + "</span>;";
		to_discover.innerHTML += " (" + str_name1 + ");";
		str_name2 = " <span onclick=\"remove" + function_suffix + "('" + name2 + "')\">" + name2 + "</span>;";
		discovered.innerHTML += " " + str_name2 + " ";
	    }

	    else
		alert("Go away, you should not be here!");

	    continue
	}

	// deal with normal case
	var str_name;
	var nber = elements[i].attributes["number"];
	if (nber != undefined && nber.nodeValue != "1")
	    name = nber.nodeValue + "x " + name;

	if (list_known.indexOf(name) == -1) {
	    str_name = " <span onclick=\"add" + function_suffix + "('" + name + "')\">" + name + "</span>;";
	    to_discover.innerHTML += str_name;
	}
	else {
	    str_name = " <span onclick=\"remove" + function_suffix + "('" + name + "')\">" + name + "</span>;";
	    discovered.innerHTML += str_name;
	}

	
    }
}

/**
 * Get corresponding element for a choice
 * @param {HTMLCollection} elements - HTML containing a list of element
 * @param {int} choice - current choice
 * @param {string} current - Name of current choice
 */
function getCorrespondingChoice(elements, choice, current) {
    for(var i=0; i < elements.length; i++) {
	var tmp_choice = elements[i].attributes["choice"];
	if (tmp_choice != undefined) {
	    tmp_choice = tmp_choice.nodeValue;
	    if (choice == tmp_choice) {
		var name = elements[i].attributes["name"].nodeValue;
		if (name != current)
		    return elements[i];
	    }
	}
    }

    alert("Unable to find corresponding choice for " + current);
    return undefined;
}


/**
 * Set a field from its ID
 * @param {career} Career - career to use
 * @param {string} id - ID to modify
 * @param {boolean} main - is it a main stat
 */
function setDataInnerHTML(career, id, main) {
    // get tag
    var x = document.getElementById("data_" + id);
    // get data
    var tmp = career.getElementsByTagName(id)[0];
    if (tmp == undefined)
	alert("XML career '" + career.id + "' is missing data '" + id + "'");
    var value = tmp.innerHTML;
    if (main && value != "")
	value += "%";
    if (value == "")
	value = "-";
    // update tag
    x.innerHTML = value;

    // now deal with character
    // get tag
    x = document.getElementById(id);
    // get correct career
    var ch_career = false;
    for(var i=0; i < character.careers.length; i++) {
	if (character.careers[i].name == career.id) {
	    ch_career = character.careers[i];
	}
    }
    if (ch_career != false) {
	value = ch_career[id];
    }

    if (ch_career == false || value == "") {
	value = 0;
    }
    x.value = value;
}

/**
 * Save current career
 * @param {option} career_to_save - Career to save
 */
function saveCareer(career_to_save) {
    var x = document.getElementById("form_career");
    var careers = character.careers;
    var career = false;
    var i;
    for(i = 0; i < careers.length; i++) {
	if (careers[i].name == career_to_save.value) {
	    career = careers[i];
	    break;
	}
    }

    // check if known
    var known = x.known.checked;
    if (!known && career != false) {
	careers.splice(i, 1);
    }

    if (career == false) {
	career = new Object();
	career.name = career_to_save.value;
	if (known) {
	    careers.push(career);
	}
	else
	    return undefined;
    }

    else {
    }

    // save main stats
    career.weapon_skill = document.getElementById("weapon_skill").value;
    career.ballistic_skill = document.getElementById("ballistic_skill").value;
    career.strength = document.getElementById("strength").value;
    career.toughness = document.getElementById("toughness").value;
    career.agility = document.getElementById("agility").value;
    career.intelligence = document.getElementById("intelligence").value;
    career.will_power = document.getElementById("will_power").value;
    career.fellowship = document.getElementById("fellowship").value;

    // save secondary stats
    career.attacks = document.getElementById("attacks").value;
    career.wounds = document.getElementById("wounds").value;
    career.strength_bonus = document.getElementById("strength_bonus").value;
    career.toughness_bonus = document.getElementById("toughness_bonus").value;
    career.movement = document.getElementById("movement").value;
    career.magic = document.getElementById("magic").value;
    career.insanity_point = document.getElementById("insanity_point").value;
    career.fate_point = document.getElementById("fate_point").value;

    return undefined;
}


/**
 * Construct the Career part of a character object 
 * @param {Character} character - Object to construct
 */
function constructorCareerPart(character) {
    saveCareer(previous_career);
}

/**
 * Generate Career document
 */
function generateCareerDocument() {
    loadCareer(previous_career);
}



/**
 * Generate a list of careers
 */
function generateListCareers() {

    var x = document.getElementById("careers");

    // clean select
    var length = x.options.length;
    for (i = 0; i < length; i++) {
	x.options[i] = null;
    }

    // add all careers
    var careers = career_parser.getElementsByTagName("career");
    for(i=0; i < careers.length; i++) {
	var key = careers[i].id;
	var option = document.createElement("option");
	option.text = key;
	option.value = key;
	x.appendChild(option);
    }

}
