/**
 * Construct the Character part of a character object 
 * @param {Character} character - Object to construct
 */
function constructorCharacterPart(character) {
    var x = document.getElementById("form_char");
 
    character.name = x.name.value;
    character.race = x.race.value;

    // Main skills
    character.weapon_skill = x.weapon_skill.value;
    character.ballistic_skill = x.ballistic_skill.value;
    character.strength = x.strength.value;
    character.toughness = x.toughness.value;
    character.agility = x.agility.value;
    character.intelligence = x.intelligence.value;
    character.will_power = x.will_power.value;
    character.fellowship = x.fellowship.value;

    // Secondary skills
    character.attacks = x.attacks.value;
    character.wounds = x.wounds.value;
    character.strength_bonus = x.strength_bonus.value;
    character.toughness_bonus = x.toughness_bonus.value;
    character.movement = x.movement.value;
    character.magic = x.magic.value;
    character.insanity_point = x.insanity_point.value;
    character.fate_point = x.fate_point.value;

    character.total_xp = x.total_xp.value;
    character.free_xp = x.free_xp.value;

    return character;

}

/**
 * Generate character tab from a Character
 */
function generateCharacterDocument(character) {
    var x = document.getElementById("form_char");

    x.name.value = character.name;
    x.race.value = character.race;

    // Main skills
    x.weapon_skill.value = character.weapon_skill;
    x.ballistic_skill.value = character.ballistic_skill;
    x.strength.value = character.strength;
    x.toughness.value = character.toughness;
    x.agility.value = character.agility;
    x.intelligence.value = character.intelligence;
    x.will_power.value = character.will_power;
    x.fellowship.value = character.fellowship;

    // Secondary
    x.attacks.value = character.attacks;
    x.wounds.value = character.wounds;
    x.strength_bonus.value = character.strength_bonus;
    x.toughness_bonus.value = character.toughness_bonus;
    x.movement.value = character.movement;
    x.magic.value = character.magic;
    x.insanity_point.value = character.insanity_point;
    x.fate_point.value = character.fate_point;

    x.total_xp.value = character.total_xp;
    x.free_xp.value = character.free_xp;
}
